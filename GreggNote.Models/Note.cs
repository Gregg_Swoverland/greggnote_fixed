﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace GreggNote.Models
{
    public class Note
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Contents { get; set; }

        [Required]
        public Guid ApplicationUserId { get; set; }

        public DateTime? DateCreated { get; set; }

        public DateTime? DateModified { get; set; }

        public int? GroupId { get; set; } 
        
        public virtual Group Group { get; set; }

        public bool IsFavorite { get; set; }

    }
}
