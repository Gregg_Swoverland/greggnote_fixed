﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace GreggNote.Models.ViewModels
{
    public class NoteGroupListViewModel
    {
        [Key]
        public int Id { get; set; }

        public string Name{ get; set; }
    }
}
