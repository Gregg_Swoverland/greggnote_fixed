﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace GreggNote.Models.ViewModels
{
    public class GroupEditViewModel
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MinLength(3, ErrorMessage = "Must be at least 3 characters.")]
        [MaxLength(128)]
        public string GroupName { get; set; }
                
    }
}
