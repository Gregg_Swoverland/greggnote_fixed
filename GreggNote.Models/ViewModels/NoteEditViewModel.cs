﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace GreggNote.Models.ViewModels
{
    public class NoteEditViewModel
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MinLength(2, ErrorMessage="Must be at least 2 characters.")]
        [MaxLength(128)]
        public string Name { get; set; }

        [Required]
        [MaxLength(8000)]
        public string Contents { get; set; }

        public int GroupId { get; set; }

        public Group Group { get; set; }

        public string GroupName { get; set; }

    }
}
