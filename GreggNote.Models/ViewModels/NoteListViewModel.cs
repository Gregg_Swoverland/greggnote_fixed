﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace GreggNote.Models.ViewModels
{
    public class NoteListViewModel
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        
        [Display(Name ="DateCreated")]
        public DateTime? DateCreated { get; set; }

        [Display(Name="DateModified")]
        public DateTime? DateModified { get; set; }

        [DefaultValue(false)]
        public bool IsFavorite { get; set; }

        public int? GroupId { get; set; }

        public Group Group { get; set; } 

        public string GroupName { get; set; }

    }
}
