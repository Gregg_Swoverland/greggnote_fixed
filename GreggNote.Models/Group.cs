﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreggNote.Models
{
    public class Group
    {
        [Key]
        public int GroupId { get; set; }

        [Display(Name ="Group Name")]
        public string GroupName { get; set; }

        public virtual ICollection<Note> Notes { get; set; }

        [Required]
        public Guid ApplicationUserId { get; set; }
    }
}
