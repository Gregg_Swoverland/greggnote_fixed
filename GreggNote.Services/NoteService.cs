﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GreggNote.Models;
using GreggNote.Models.ViewModels;
using GreggNote.DataAccess;

namespace GreggNote.Services
{
    public class NoteService
    {
        public bool Create(NoteEditViewModel model, Guid userId) 
        {
            using (var dataContext = new GreggNoteDataContext()) 
            {
                var note = new Note();
                note.ApplicationUserId = userId;
                note.Name = model.Name;
                note.Contents = model.Contents;
                note.DateCreated = DateTime.Now;
                //note.GroupId = -1;
                //note.GroupId = model.GroupId;

                dataContext.Notes.Add(note);
                return ( 1 == dataContext.SaveChanges() );
            }
        }

        public bool CreateGroup(GroupEditViewModel model, Guid userId)
        {
            using (var dataContext = new GreggNoteDataContext())
            {
                var group = new Group();
                group.GroupName = model.GroupName;
                group.ApplicationUserId = userId;

                dataContext.Groups.Add(group);
                return (1 == dataContext.SaveChanges());
            }
        }

        // TODO: Make this DRY?
        public List<NoteListViewModel> GetAllForUser(Guid userId, string searchString = "")
        {
            using (var dataContext = new GreggNoteDataContext())
            {
                if (!("" == searchString))
                {
                    var result = (from note in dataContext.Notes
                                  where note.ApplicationUserId == userId
                                  && note.Contents.Contains(searchString)
                                  select new NoteListViewModel()
                                  {
                                      DateCreated = note.DateCreated.Value,
                                      DateModified = note.DateModified,
                                      Id = note.Id,
                                      Name = note.Name, 
                                      IsFavorite = note.IsFavorite,
                                      GroupId = note.GroupId
                                  }).ToList();
                    return result;
                }
                var result2 = (from note in dataContext.Notes
                              where note.ApplicationUserId == userId
                              select new NoteListViewModel()
                                  {
                                      DateCreated = note.DateCreated.Value,
                                      DateModified = note.DateModified,
                                      Id = note.Id,
                                      Name = note.Name,
                                      IsFavorite = note.IsFavorite,
                                      GroupId = note.GroupId
                                  }).ToList();
                
                return result2;
            }
        }

        public List<NoteListViewModel> GetAllForUserByGroup(Guid userId, int groupId)
        {
            using (var dataContext = new GreggNoteDataContext())
            {
                {
                    var result = (from note in dataContext.Notes
                                  where note.ApplicationUserId == userId
                                  && note.GroupId == groupId
                                  select new NoteListViewModel()
                                  {
                                      DateCreated = note.DateCreated.Value,
                                      DateModified = note.DateModified,
                                      Id = note.Id,
                                      Name = note.Name
                                  }).ToList();
                    return result;
                }
                
            }
        }

        public NoteEditViewModel GetById(int id, Guid userId)
        {
            using (var context = new GreggNoteDataContext())
            {
                var note = context.Notes.Where(n => n.ApplicationUserId == userId && n.Id == id).SingleOrDefault();

                
                if (note == null) return null;  // HACK: Workaround for 'note/edit/non-existent note'

                var result = new GreggNote.Models.ViewModels.NoteEditViewModel()
                {
                    Contents = note.Contents,
                    Id = note.Id,
                    GroupId = note.GroupId == null ? -1 : note.GroupId.Value,
                    Name = note.Name,
                    //GroupId = note.GroupId
                };

                return result; 
            }
        }

        public bool Update(NoteEditViewModel model, Guid userId)
        {
            using (var context = new GreggNoteDataContext())
            {
                // Attempt to get the note from the database.
                var note = context.Notes.Where(w => w.Id == model.Id && w.ApplicationUserId == userId).SingleOrDefault();

                // Check if note is valid
                if (note == null) return false;

                // Update the note.
                note.Contents = model.Contents;
                note.Name = model.Name;
                note.DateModified = DateTime.Now;
                note.GroupId = model.GroupId;

                // Save changes to the database.
                var result = context.SaveChanges();
                return result == 1;
            }
        }

        public bool Delete(int id, Guid userId)
        {
            using (var context = new GreggNoteDataContext())
            {
                // Attempt to get the note from the database.  
                var note = context.Notes.Where(w => w.Id == id &&
                    w.ApplicationUserId == userId).SingleOrDefault();

                // var note2 = (from w in context.Notes
                //      where w.Id == ... select w).SingleOrDefault();

                // Make sure a note was received before updating
                if (note == null) return false;

                // Delete the note.
                context.Notes.Remove(note);

                // Save the changes to the database.
                var result = context.SaveChanges();

                // Return the result.
                return 1 == result;
            }

        } //End Delete()
  
        public bool ToggleFavorite(int id, Guid userId)
        {
            using(var context = new GreggNoteDataContext())
            {
                var note = context.Notes.Where(w => w.Id == id &&
                    w.ApplicationUserId == userId).SingleOrDefault();

                if (note == null) return false;

                note.IsFavorite = !note.IsFavorite;

                var result = context.SaveChanges();

                return result == 1;
            }
        }
    }
}
