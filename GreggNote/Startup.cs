﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GreggNote.Web.Startup))]
namespace GreggNote.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
