﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GreggNote.Models.ViewModels;
using Microsoft.AspNet.Identity;
using GreggNote.Services;
using GreggNote.Models;
using GreggNote.DataAccess;


namespace GreggNote.Controllers
{
    [Authorize]
    //[RequireHttps]
    public class NotesController : Controller
    {
        GreggNoteDataContext dataContext;
        public NotesController()
        {
            dataContext = new GreggNoteDataContext();
        }
        //NoteService for class 
        readonly NoteService _service = new NoteService(); 

        Guid CurrentUserId
        {
            get
            {
                return new Guid(User.Identity.GetUserId());
            }
        }
               
        public enum Sort_by { name, created, modified };
        // Set initial sort
        public static Sort_by last_sort = Sort_by.name;
        public static bool sortAscend = true;
        public static bool isGroupView;

        public ActionResult Index(string searchString = "", string sort ="")
        {
            var notes = _service.GetAllForUser(this.CurrentUserId, searchString);
            isGroupView = false;
            // Send appropriate message if notes are returned
            if (!notes.Any() && !("" == searchString))
            {
               TempData.Clear();
               TempData["ErrorMessage"] = "That text was not found in any note contents.";    
            }

            switch (sort)
            {
                case "name":
                    if (last_sort == Sort_by.name && sortAscend)
                    {
                        notes = notes.OrderByDescending(o => o.Name).ToList();
                        last_sort = Sort_by.name;
                        sortAscend = false;
                        break;
                    }
                    notes = notes.OrderBy(o => o.Name).ToList();
                    last_sort = Sort_by.name;
                    sortAscend = true;
                    break;
                case "modified":
                    if (last_sort == Sort_by.modified && sortAscend)
                    {
                        notes = notes.OrderByDescending(o => o.DateModified).ToList();
                        last_sort = Sort_by.modified;
                        sortAscend = false;
                        break;
                    }
                    notes = notes.OrderBy(o => o.DateModified).ToList();
                    last_sort = Sort_by.modified;
                    sortAscend = true;
                    break;
                case "created":
                    if (last_sort == Sort_by.created && sortAscend)
                    {
                        notes = notes.OrderByDescending(o => o.DateCreated).ToList();
                        last_sort = Sort_by.created;
                        sortAscend = false;
                        break;
                    }
                    notes = notes.OrderBy(o => o.DateCreated).ToList();
                    last_sort = Sort_by.created;
                    sortAscend = true;
                    break;
                default:
                    notes = notes.OrderBy(o => o.Name).ToList();
                    last_sort = Sort_by.name;
                    sortAscend = true;
                    break;
            }

            ViewBag.SortBy = last_sort.ToString();
            ViewBag.sortAscend = sortAscend;
            
            var groupList = (from g in dataContext.Groups
                              where g.ApplicationUserId == CurrentUserId
                             select g).ToList();
            ViewBag.groups = groupList;
            return View(notes);
        }

        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet()
        {
            return View(new NoteEditViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        [ActionName("Create")]
        public ActionResult CreatePost(NoteEditViewModel editModel)
        {
            if (ModelState.IsValid)
            {
                _service.Create(editModel, this.CurrentUserId);
                return RedirectToAction("Index");
            }
            return View(editModel);
        }

        [HttpGet]
        [ActionName("Details")]
        public ActionResult Details(int id)
        {
            if (-1 == id)
            {
                TempData.Clear(); //Prevent 'same key exists' exception
                TempData.Add("ErrorMessage", "Please specify a note to view.");
                return RedirectToAction("Index");
            }
            var note = _service.GetById(id, this.CurrentUserId);
            
            if (null == note) return HttpNotFound();

            var gL = dataContext.Groups.ToList();
            var gName = note.GroupId < 1 ? "" : gL[note.GroupId-1].GroupName;
            ViewBag.gName = gName;
            return View(note);
        }

        [HttpGet]
        [ActionName("Edit")]
        public ActionResult EditGet(int id = -1)
        {
            if (-1 == id) 
            {
                TempData.Clear(); //Prevent 'same key exists' exception
                TempData.Add("ErrorMessage", "Please specify a note to edit.");
                return RedirectToAction("Index");
            }

            var note = _service.GetById(id, this.CurrentUserId);
            if (note == null)
            {
                TempData.Clear(); //Prevent 'same key exists' exception
                TempData.Add("ErrorMessage", "That note couldn't be found.");
                return RedirectToAction("Index");
            }

            var groupList = (from g in dataContext.Groups
                             where g.ApplicationUserId == CurrentUserId
                             select g).ToList();
            ViewBag.GroupList = groupList;
                    
            return View(note);
        }

        [HttpPost]
        [ActionName("Edit")]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult EditPost(NoteEditViewModel model)
        {
            if (ModelState.IsValid)
            {
                _service.Update(model, this.CurrentUserId);
                return RedirectToAction("Index");
            }
            return View(model);
        }

        [HttpGet]
        [ActionName("Delete")]
        public ActionResult DeleteGet(int id)
        {
            // Attempt to get the note we're editing.
            var note = _service.GetById(id, this.CurrentUserId);
            
            // Make sure we have a note.
            if (note == null) return HttpNotFound();

            // If all good, pass note to view.
            return View(note);
        }

        [HttpPost]
        [ActionName("Delete")]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult DeletePost(int id)
        {
            if (ModelState.IsValid)
            {
                var note = _service.Delete(id, this.CurrentUserId);
            }

            return RedirectToAction("Index");
        }
        
        // [HttpGet]
        public ActionResult Toggle(int id = -1) 
        {
            if (id == -1)
            {
                TempData["ErrorMessage"] = "No note was specified.";
                return RedirectToAction("Index");
            }

            var note = _service.GetById(id, this.CurrentUserId);

            if (note == null)
            {
                TempData["ErrorMessage"] = "That note couldn't be found.";
                return RedirectToAction("Index");            
            }

            _service.ToggleFavorite(id, this.CurrentUserId);
            return RedirectToAction("Index");
        }

        public ActionResult GetAllInGroup(int id)
        {
            
            var notes = _service.GetAllForUserByGroup(this.CurrentUserId, id);
            isGroupView = true;
            if (!notes.Any())
            {
                TempData.Clear();
                TempData["ErrorMessage"] = "That note group is empty.";
                return RedirectToAction("Index");
            }
            ViewData["IsGroupView"] = isGroupView;
            return View("Index" , notes);
        }

        [HttpGet]
        [ActionName("CreateGroup")]
        public ActionResult CreateGroupGet()
        {
            return View(new GroupEditViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        [ActionName("CreateGroup")]
        public ActionResult CreateGroupPost(GroupEditViewModel editModel)
        {
            if (ModelState.IsValid)
            {
                _service.CreateGroup(editModel, this.CurrentUserId);
                return RedirectToAction("Index");
            }
            return View(editModel);
        }

    }
}