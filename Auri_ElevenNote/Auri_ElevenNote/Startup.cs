﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Auri_ElevenNote.Startup))]
namespace Auri_ElevenNote
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
